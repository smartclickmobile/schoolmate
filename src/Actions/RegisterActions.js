import axios from "axios"
import { 
  RegisterSuccess, RegisterError
} from "./type"
import { Alert, AsyncStorage } from "react-native"
import { Actions } from "react-native-router-flux";
import { API_IP } from '../Constant/constant'
export const registeruser = option => {
  // console.warn('registeruser')
    return async dispatch => {
      const config = {
        headers: {
          "content-type": "multipart/form-data"
        }
      }
      const data = new FormData()
      data.append("register_type", 'STUDENT_PARENT_ACCOUNT')
      data.append("parent_username", option.parent_username)
      data.append("parent_firstname", option.parent_firstname)
      data.append("parent_lastname", option.parent_lastname)
      data.append("parent_citizen_number", option.parent_citizen_number)
      data.append("parent_tel", option.parent_tel)
      data.append("parent_address", option.parent_address)
      data.append("parent_password", option.parent_password)
      data.append("parent_retype_password", option.parent_retype_password)
      data.append("student_firstname", option.student_firstname)
      data.append("student_lastname", option.student_lastname)
      data.append("student_citizen_number", option.student_citizen_number)
      data.append("student_birth_date", option.student_birth_date)
      // data.append("image", option.image)
      // console.warn('data',option)
      axios
        .post(API_IP + "auth/register", data, config)
        .then(function (response) {
          console.warn(response.data)
          if(response.data.code == '0x0000-00000'){
            Actions.pop()
            // dispatch({ type: LoginSuccess, payload: response.data.message })
          }else{
            Alert.alert('Register fail',response.data.message, [{ text: 'OK', onPress: null }])
            // dispatch({ type: LoginError, payload: response.data.message })
          }
        })
        .catch(function (error) {
          console.warn('error api',error)
        //   firebase.crashlytics().log('step10/removeconcreteregister')
        //   firebase.crashlytics().recordError(error.response ? error.response.status : 0, error.message)
        //   dispatch({ type: PILE_PILE09_ERROR, payload: error.message })
        })
    }
  }