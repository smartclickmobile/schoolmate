export const CHECKTYPEIPHONE = 'check_type_iphone'
export const CHECKTYPEIPHONE_ERROR = 'check_type_iphone_error'

export const RegisterSuccess = 'register'
export const RegisterError = 'registererror'

export const LoginSuccess = 'login'
export const LoginError = 'loginrerror'
export const LogoutSuccess = 'logout'
export const LogoutError = 'logoutrerror'
export const UserSuccess = 'user'
export const UserError = 'userrerror'

export const StudentSuccess = 'student'
export const StudentError = 'studenterror'
export const EnrollSuccess = 'enroll'
export const EnrollError = 'enrollerror'
export const GradeSuccess = 'grade'
export const GradeError = 'gradeerror'

export const NewsSuccess = 'news'
export const NewsError = 'newserror'
export const NewsdetailSuccess = 'newsdetail'
export const NewsdetailError = 'newsdetailserror'

export const CalendarSuccess = 'calendar'
export const CalendarError = 'calendarerror'

export const LOADING = 'loading'
export const LOADINGNEWS = 'loadingnews'
export const LOADINGCAL = 'loadingcal'
