import axios from "axios"
import { 
    StudentSuccess, StudentError,
    EnrollSuccess, EnrollError,
    GradeSuccess, GradeError,
    LOADING
} from "./type"
import { Alert, AsyncStorage } from "react-native"
import { Actions } from "react-native-router-flux";
import { API_IP } from '../Constant/constant'
export const getStudent = data => {
    return async dispatch => {
      const config = {
        headers: {
          "content-type": "application/x-www-form-urlencoded"
        }
      }
      
      axios
        .get(API_IP + "my/student", config)
        .then(function (response) {
        console.warn("response student", response.data)
          if(response.data.code == '0x0000-00000'){
            dispatch({ type: StudentSuccess, payload: response.data.data })
          }else{
            AsyncStorage.setItem('login', 'false');
            Alert.alert('error','section หมดอายุ', [
                { 
                    text: 'OK', 
                    onPress:()=> Actions.login() }])
           
          }
        //   dispatch({ type: PILE_ERROR, payload: error.message })
        })
        .catch(function (error) {
          console.log(error.response)
        //   firebase.crashlytics().log('getStudent')
        //   firebase.crashlytics().recordError(error.response ? error.response.status : 0, error.message)
        //   dispatch({ type: PILE_ERROR, payload: error.message })
        })
    }
  }
  export const getEnroll = data => {
    return async dispatch => {
      const config = {
        headers: {
          "content-type": "application/x-www-form-urlencoded"
        }
      }
      // console.log("pileMasterInfoData", data)
      axios
        .get(API_IP + "my/enrolls", config)
        .then(function (response) {
        console.warn("response Enroll", response.data)
          if(response.data.code == '0x0000-00000'){
            dispatch({ type: EnrollSuccess, payload: response.data.data })
          }else{
            // Alert.alert('error','section หมดอายุ', [
            //     { 
            //         text: 'OK', 
            //         onPress: Actions.pop() }])
           
          }
        //   dispatch({ type: PILE_ERROR, payload: error.message })
        })
        .catch(function (error) {
          console.log(error.response)
        //   firebase.crashlytics().log('getStudent')
        //   firebase.crashlytics().recordError(error.response ? error.response.status : 0, error.message)
        //   dispatch({ type: PILE_ERROR, payload: error.message })
        })
    }
  }
  export const getGrade = data => {
    return async dispatch => {
      const config = {
        headers: {
          "content-type": "application/x-www-form-urlencoded"
        }
      }
      // console.log("pileMasterInfoData", data)
      axios
        .get(API_IP + "assessment/summary", config)
        .then(function (response) {
            // console.warn("response", response.data)
          if(response.data.code == '0x0000-00000'){
            dispatch({ type: GradeSuccess, payload: response.data.data })
          }else{
            // Alert.alert('error','section หมดอายุ', [
            //     { 
            //         text: 'OK', 
            //         onPress: Actions.pop() }])
           
          }
        //   dispatch({ type: PILE_ERROR, payload: error.message })
        })
        .catch(function (error) {
          console.log(error.response)
        //   firebase.crashlytics().log('getStudent')
        //   firebase.crashlytics().recordError(error.response ? error.response.status : 0, error.message)
        //   dispatch({ type: PILE_ERROR, payload: error.message })
        })
    }
  }
  export const setLoading = data => {
    return async dispatch => {
      dispatch({ type: LOADING })  
    }
  }