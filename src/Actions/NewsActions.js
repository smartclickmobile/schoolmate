import axios from "axios"
import { 
    NewsSuccess, NewsError,
    NewsdetailSuccess, NewsdetailError,
    LOADINGNEWS
} from "./type"
import { Alert, AsyncStorage } from "react-native"
import { Actions } from "react-native-router-flux";
import { API_IP } from '../Constant/constant'
export const getNews = option => {
    return async dispatch => {
      const config = {
        headers: {
          "content-type": "multipart/form-data"
        }
      }
      console.warn('getuserinfo')
      axios
        .get(API_IP + "feed", config)
        .then(function (response) {
          console.warn(response.data)
          if(response.data.code == '0x0000-00000'){
            dispatch({ type: NewsSuccess, payload: response.data.data })
          }else{
            AsyncStorage.setItem('login', 'false');
            Alert.alert('error','section หมดอายุ', [
                { 
                    text: 'OK', 
                    onPress:()=> Actions.login() }])
          }
        })
        .catch(function (error) {
          console.warn(error)
        //   firebase.crashlytics().log('step10/removeconcreteregister')
        //   firebase.crashlytics().recordError(error.response ? error.response.status : 0, error.message)
        //   dispatch({ type: PILE_PILE09_ERROR, payload: error.message })
        })
    }
  }

  export const getNewsdetail = option => {
    return async dispatch => {
      const config = {
        headers: {
          "content-type": "multipart/form-data"
        }
      }
      // console.warn('getuserinfo')
      axios
        .get(API_IP + "feed/get/"+option, config)
        .then(function (response) {
          // console.warn('new detail',option,response.data.data)
          if(response.data.code == '0x0000-00000'){
            dispatch({ type: NewsdetailSuccess, payload: response.data.data })
          }else{
           
          }
        })
        .catch(function (error) {
          console.warn(error)
        //   firebase.crashlytics().log('step10/removeconcreteregister')
        //   firebase.crashlytics().recordError(error.response ? error.response.status : 0, error.message)
        //   dispatch({ type: PILE_PILE09_ERROR, payload: error.message })
        })
    }
  }
  export const setLoadingNews = data => {
    return async dispatch => {
      dispatch({ type: LOADINGNEWS })  
    }
  }