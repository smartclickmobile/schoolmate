import axios from "axios"
import { 
  LoginSuccess, LoginError,
  UserSuccess, UserError
} from "./type"
import { Alert, AsyncStorage } from "react-native"
import { Actions } from "react-native-router-flux";
import { API_IP } from '../Constant/constant'
export const Loginuser = option => {
    return async dispatch => {
      const config = {
        headers: {
          "content-type": "multipart/form-data"
        }
      }
      const data = new FormData()
      data.append("login_type", 'STUDENT_PARENT_ACCOUNT')
      data.append("username", option.username)
      data.append("password", option.password)
  
      axios
        .post(API_IP + "auth/login", data, config)
        .then(function (response) {
          // console.warn('res',response)
          if(response.data.code == '0x0000-00000'){
            getuserinfo()
            AsyncStorage.setItem('login', 'true');
            Actions.dashborad()
            // dispatch({ type: LoginSuccess, payload: response.data.message })
          }else{
            Alert.alert('login fail',response.data.message, [{ text: 'OK', onPress: null }])
            // dispatch({ type: LoginError, payload: response.data.message })
          }
        })
        .catch(function (error) {
          console.warn(error)
        //   firebase.crashlytics().log('step10/removeconcreteregister')
        //   firebase.crashlytics().recordError(error.response ? error.response.status : 0, error.message)
        //   dispatch({ type: PILE_PILE09_ERROR, payload: error.message })
        })
    }
  }

  export const Logoutuser = option => {
    
    return async dispatch => {
      const config = {
        headers: {
          "content-type": "multipart/form-data"
        }
      }
      // const data = new FormData()
      // data.append("login_type", 'STUDENT_PARENT_ACCOUNT')
      // data.append("username", option.username)
      // data.append("password", option.password)
      console.log('Logoutuser')
      axios
        .post(API_IP + "auth/logout", config)
        .then(function (response) {
          // console.warn(response.data)
          if(response.data.code == '0x9000-00000'){
            AsyncStorage.setItem('login', 'false');
            Actions.login()

            // dispatch({ type: LoginSuccess, payload: response.data.message })
          }else{
            Alert.alert('logout fail',response.data.message, [{ text: 'OK', onPress: null }])
            // dispatch({ type: LoginError, payload: response.data.message })
          }
        })
        .catch(function (error) {
          console.warn(error)
        //   firebase.crashlytics().log('step10/removeconcreteregister')
        //   firebase.crashlytics().recordError(error.response ? error.response.status : 0, error.message)
        //   dispatch({ type: PILE_PILE09_ERROR, payload: error.message })
        })
    }
  }

  export const getuserinfo = option => {
    return async dispatch => {
      const config = {
        headers: {
          "content-type": "multipart/form-data"
        }
      }
      // console.warn('getuserinfo')
      axios
        .get(API_IP + "my", config)
        .then(function (response) {
          console.warn('getuserinfo',response.data)
          if(response.data.code == '0x0000-00000'){
            dispatch({ type: UserSuccess, payload: response.data.data })
          }else{
            // Alert.alert('logout fail',response.data.message, [{ text: 'OK', onPress: null }])
            // dispatch({ type: LoginError, payload: response.data.message })
          }
        })
        .catch(function (error) {
          console.warn(error)
        //   firebase.crashlytics().log('step10/removeconcreteregister')
        //   firebase.crashlytics().recordError(error.response ? error.response.status : 0, error.message)
        //   dispatch({ type: PILE_PILE09_ERROR, payload: error.message })
        })
    }
  }