import axios from "axios"
import { 
    CalendarSuccess, CalendarError,
    LOADINGCAL
} from "./type"
import { Alert, AsyncStorage } from "react-native"
import { Actions } from "react-native-router-flux";
import { API_IP } from '../Constant/constant'
export const getCalendar = option => {
    return async dispatch => {
      const config = {
        headers: {
          "content-type": "multipart/form-data"
        }
      }
      // console.warn('getuserinfo')
      axios
        .get(API_IP + "calendar", config)
        .then(function (response) {
          console.warn('getCalendar',response.data)
          if(response.data.code == '0x0000-00000'){
            dispatch({ type: CalendarSuccess, payload: response.data.data })
          }else{
            AsyncStorage.setItem('login', 'false');
            Alert.alert('error','section หมดอายุ', [
                { 
                    text: 'OK', 
                    onPress:()=> Actions.login() }])
          }
        })
        .catch(function (error) {
          console.warn(error)
        //   firebase.crashlytics().log('step10/removeconcreteregister')
        //   firebase.crashlytics().recordError(error.response ? error.response.status : 0, error.message)
        //   dispatch({ type: PILE_PILE09_ERROR, payload: error.message })
        })
    }
  }
  export const setLoadingCal = data => {
    return async dispatch => {
      dispatch({ type: LOADINGCAL })  
    }
  }