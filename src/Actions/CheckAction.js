import {
    CHECKTYPEIPHONE,
    CHECKTYPEIPHONE_ERROR,
} from './type'
export const checkiphonex = (data) => {
    return async dispatch => {
        if(data.height==812&&data.width==375){
            return dispatch({ type: CHECKTYPEIPHONE, payload: 'x' })
        }else if(data.height==896&&data.width==414){
            return dispatch({ type: CHECKTYPEIPHONE, payload: 'xr' })
        }else{
            return dispatch({ type: CHECKTYPEIPHONE, payload: 'nox' })
        }
    }
}