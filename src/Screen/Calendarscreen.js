
import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Dimensions,ActivityIndicator} from 'react-native';
import { Calendar, CalendarList, Agenda,LocaleConfig } from 'react-native-calendars';
import { MainColor,SubColor,BaseColor } from '../../Assets/color'
import * as actions from "../Actions"
import {connect} from "react-redux"
import moment from "moment"
LocaleConfig.locales['th']= {
  monthNames: ['มกราคม','กุมภาพันธ์','มีนาคม','เมษายน','พฤษภาคม','มิถุนายน','กรกฏาคม','สิงหาคม','กันยายน','ตุลาคม','พฤศจิกายน','ธันวาคม'],
  monthNamesShort: ['ม.ค.','ก.พ.','มี.ค.','เม.ย.','พ.ค.','มิ.ย.','ก.ค.','ส.ค.','ก.ย.','ต.ค.','พ.ย.','ธ.ค.'],
  dayNames: ['อาทิตย์','จันทร์','อังคาร','พุธ','พฤศจิกายน','ศุกร์','เสาร์'],
  dayNamesShort: ['อา.','จ.','อ.','พ.','พฤ.','ศ.','ส.']
}
LocaleConfig.defaultLocale = 'th';
class Calendarscreen extends Component{
  constructor(props){
    super(props)
    this.state={
      dayselect:'',
      calendarlist:[],
      markedDates:'',
      dateselect:'',
      dateinfo:'',
      month:['มกราคม','กุมภาพันธ์','มีนาคม','เมษายน','พฤษภาคม','มิถุนายน','กรกฏาคม','สิงหาคม','กันยายน','ตุลาคม','พฤศจิกายน','ธันวาคม'],
      day:'',
      monthselect:'',
      year:'',
      today:'',
      loading:false
    }
  }
  componentDidMount(){
    this.props.getCalendar()
    var date = new Date()
    var day = moment(date).format('YYYY-MM-DD')
    this.setState({today:day})
  }
  componentWillReceiveProps(nextProps){
    // console.warn(new Date())
    if(nextProps.CalendarSuccess != null && nextProps.CalendarSuccess != undefined && this.state.calendarlist == ''){
      this.setState({calendarlist:nextProps.CalendarSuccess},()=>{
        var value = {}
        // console.warn('datedate',moment(this.state.calendarlist[0].date).format('YYYY-MM-DD'))
       for(var i = 0; i<this.state.calendarlist.length; i++){
          var date = moment(this.state.calendarlist[i].date).format('YYYY-MM-DD')
          value[date] = {marked:true, dotColor: '#eb4d4b'}
       }

      //  console.warn('sss',value)
       this.setState({dayselect:value })
      })
    }
    if(nextProps.loading != null && nextProps.loading != undefined){
      this.setState({loading:nextProps.loading})
    }
  }
  setDayselect(day){
    var value = {}
        // console.warn('datedate',moment(this.state.calendarlist[0].date).format('YYYY-MM-DD'))
       for(var i = 0; i<this.state.calendarlist.length; i++){
          var date = moment(this.state.calendarlist[i].date).format('YYYY-MM-DD')
          value[date] = {marked:true, dotColor: '#eb4d4b'}
       }
       value[day.dateString] = {selected: true, selectedColor: MainColor}
    this.setState({dayselect:value})
    var data = []
    var check = false
     for(var i =0;i<this.state.calendarlist.length;i++){
      if(moment(this.state.calendarlist[i].date).format('YYYY-MM-DD') == day.dateString){
        check = true
       
        this.setState({dateinfo:this.state.calendarlist[i]},()=>{
          // console.warn('test',this.state.dateinfo)
          var date = this.state.dateinfo.date
          var datespile = date.split(" ")
          var datespiledate = datespile[0].split("-")
          var day = datespiledate[2]
          var month = datespiledate[1]
          var year = datespiledate[0]
          this.setState({day:day,monthselect:parseInt(month),year:parseInt(year)+543})
        })
      }
     }
     if(check == false){
      this.setState({dateinfo:''})
     }
     
    
  }
  renderdata(){
    return (
      // <View style={styles.container}>
      <View style={{flex:1,marginTop:Dimensions.get('window').height/100*5}}>
        <Calendar
            onDayPress={(day)=> this.setDayselect(day)}
            selectedDayColor={MainColor}
            selectedDayTextColor='#fff'
            markedDates={this.state.dayselect}
        />
        {this.state.dateinfo!=''?<View style={{flex:1,margin:Dimensions.get('window').width/100*5,backgroundColor:MainColor, borderRadius:Dimensions.get('window').width/100*7,padding:Dimensions.get('window').width/100*7}}>
          <Text style={{fontFamily:'Kanit-Bold',fontSize:Dimensions.get('window').height/100*3,color:'#fff'}}>{this.state.day+' '+this.state.month[this.state.monthselect-1]+' '+this.state.year}</Text>
          <Text style={{fontFamily:'Kanit-Regular',fontSize:Dimensions.get('window').height/100*2.5,marginTop:Dimensions.get('window').height/100*3,color:'#fff',marginLeft:Dimensions.get('window').width/100*5,color:'#fff'}}>{this.state.dateinfo.name}</Text>
        </View>:<View/>}
      </View>
       
      // </View>
    )
  }
  render() {
    // console.warn('test',this.state.dateselect != '')
    return (
      <View style={styles.container}>
      {this.state.loading?<ActivityIndicator size="large" color={MainColor} style={{alignItems:'center',marginTop:20}} />:this.renderdata()}
      </View>
    );
  }
}
const mapStateToProps = state => {
  return {
    CalendarSuccess: state.Calendar.CalendarSuccess,
    loading: state.Calendar.loading,
      // error: state.Register.error
  }
}
export default connect(mapStateToProps, actions)(Calendarscreen)
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5FCFF',
   
  },
});
