
import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,Dimensions,TouchableOpacity,Image,BackHandler,ToastAndroid, ActivityIndicator} from 'react-native';
import { Icon } from 'react-native-elements'
import {MenuColor_Text, MainColor} from '../../Assets/color'
import { Actions } from 'react-native-router-flux';
import * as actions from "../Actions"
import {connect} from "react-redux"
import { SIZE_4 } from '../Constant/font'
const dim = Dimensions.get('window');
class Dashborad extends Component {
  constructor(props){
    super(props)
    this.state = {
      studentlist:[],
      Enroll:null,
      data:[],
      loading:false
    }
  }
  componentWillReceiveProps(nextProps){
    var value = nextProps.EnrollSuccess
    console.warn('componentWillReceiveProps', nextProps.StudentSuccess)
    if(nextProps.StudentSuccess != undefined && nextProps.StudentSuccess != null){
     var data = []
     data.push(nextProps.StudentSuccess)
      this.setState({studentlist:data})
    }
    if(nextProps.EnrollSuccess != undefined && nextProps.EnrollSuccess != null){
      var data = []
      data.push(nextProps.EnrollSuccess)
      this.setState({Enroll:data},()=>{
        // console.warn('enroll',this.state.Enroll[0].class)
      })
    }
    if(nextProps.loading != undefined && nextProps.loading != null){
      this.setState({loading:nextProps.loading})
    }
  }
  async componentDidMount(){
    // console.warn('componentDidMount')
    BackHandler.addEventListener("back", () => this._handleBack())
    await this.props.getStudent()
    await this.props.getEnroll()
    await this.props.getGrade()
    await this.props.getuserinfo()
  }
  _handleBack() {
    if(Actions.currentScene == "_dashborad" || Actions.currentScene == 'dashborad') {
      if(this.state.doubleBackToExitPressedOnce) {
        BackHandler.exitApp()
      }
      ToastAndroid.show("Press back again to exit", ToastAndroid.SHORT)
      this.setState({ doubleBackToExitPressedOnce: true })
      setTimeout(() => {
        this.setState({ doubleBackToExitPressedOnce: false })
      }, 2000)
      return true
    }
    return false
  }
  renderloading(){
    return(
      <ActivityIndicator size="large" color={MainColor} style={{alignItems:'center',marginTop:20}} />
    )
  }
  renderdata(){
    return(
      <View>
      {this.state.studentlist.map((item,index)=>{
        return(
          <View key={index} style={styles.containerinput}>
            <View style={{flex:1,justifyContent:'center'}}>
              <Image
                source={item.image==null?require('../../Assets/Images/user.png'):{uri:item.image}}
                style={{
                  height:Platform.isPad? dim.width/100*20:dim.width/100*28,
                  width:Platform.isPad? dim.width/100*20:dim.width/100*28,
                  borderRadius:Platform.isPad? dim.width/100*10:dim.width/100*14,
                  borderColor:MainColor,
                  borderWidth:Platform.isPad? dim.width/100*0.5:dim.width/100*1,
                  alignSelf:'center'
                  
                }}
                resizeMode={'cover'}
              />
            </View>
            <View style={{flex:2,paddingLeft:15,justifyContent:'center'}}>
                <View style={{flexDirection:'row'}}>
                  <Text style={[styles.h1,{flex:0.4}]}>ชื่อ</Text>
                  <Text numberOfLines={1} style={[styles.text,{flex:3}]}>{item.title+' '+item.thai_firstname+' '+item.thai_lastname}</Text>
                </View>
                <View style={{flexDirection:'row',marginTop:dim.height/100*0.7}}>
                  <Text style={[styles.h1,{flex:1}]}>เลขประจำตัว</Text>
                  <Text style={[styles.text,{flex:1}]}>{item.code}</Text>
                </View>
                <View style={{flexDirection:'row',marginTop:dim.height/100*0.7}}>
                  <Text style={[styles.h1,{flex:1}]}>ชั้น</Text>
                  <Text style={[styles.text,{flex:1}]}>{this.state.Enroll==null?'':this.state.Enroll[0].class}</Text>
                </View>
                <TouchableOpacity style={styles.botton} onPress={()=> Actions.infostudent({dataprops:item,room:this.state.Enroll==null?'':this.state.Enroll[0].class})}>
                  <Text style={[styles.text,{color:'#fff'}]}>ดูข้อมูล/ผลการเรียน</Text>
                </TouchableOpacity>
            </View>
          </View>
        )
      })}
      <View style={{
        flexDirection:'row',
        padding:dim.width/100*3
      }}>
        <View style={{flex:1,alignItems:'center'}}>
          <TouchableOpacity onPress={()=> Actions.calendarscreen()} 
            style={{
              width:dim.width/100*45,
              alignItems:'center',
              backgroundColor:'#69BBF2',
              borderRadius: dim.width/100*3,
              paddingHorizontal:dim.width/100*3,
              paddingVertical:dim.width/100*6,
              
              justifyContent:'center'
            }}
          >
            <Icon name={'calendar'} type='entypo' color={'#fff'} size={dim.width/100*7}/>
            <Text style={[styles.text,{color:'#fff'}]}>ปฏิทินโรงเรียน</Text>
          </TouchableOpacity>
        </View>
        <View style={{flex:1,alignItems:'center'}}>
          <TouchableOpacity onPress={()=> Actions.news()} 
            style={{
              width:dim.width/100*45,
              alignItems:'center',
              backgroundColor:'#908CE5',
              borderRadius: dim.width/100*3,
              paddingHorizontal:dim.width/100*3,
              paddingVertical:dim.width/100*6,
              justifyContent:'center'
            }}
          >
            <Icon name={'open-book'} type='entypo' color={'#fff'} size={dim.width/100*7}/>
            <Text style={[styles.text,{color:'#fff'}]}>ข่าวประชาสัมพันธ์</Text>
          </TouchableOpacity>
        </View>
      </View>
      </View>
    )
  }
  render() {
    return (
      <View style={styles.container}>
        {this.state.loading?this.renderloading():this.renderdata()}
      </View>
    );
  }
}
const mapStateToProps = state => {
  return {
    StudentSuccess: state.student.StudentSuccess,
    studenterror: state.student.error,
    EnrollSuccess: state.student.EnrollSuccess,
    loading: state.student.loading
  }
}
export default connect(mapStateToProps, actions)(Dashborad)
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5FCFF',
  },
  container2:{
    flex:1,
    borderRadius:dim.width/100*5,
    margin:dim.width/100*2.5
  },
  container3: {
    margin:dim.width/100*2.5
  },
  containerinput: {
    width:dim.width/100*93,
    backgroundColor:'#fff',
    alignSelf:'center',
    marginTop:dim.height/100*3,
    borderWidth: 1,
    borderRadius: dim.width/100*2,
    borderColor: '#ddd',
    borderBottomWidth: 0,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 2,
    padding:dim.width/100*3,
    flexDirection:'row'
  },
  h1:{
    color:'#1C6BA0',
    fontFamily:'Kanit-Regular',
    fontSize:SIZE_4
  },
  text:{
    color:'#333',
    fontFamily:'Kanit-Regular',
    fontSize:SIZE_4
  },
  botton:{
    backgroundColor:'#1C6BA0',
    height:Platform.isPad? dim.height/100*5:dim.height/100*4,
    borderRadius:Platform.isPad? dim.width/100*2:dim.width/100*3,
    alignItems:'center',
    justifyContent:'center',
    margin:dim.width/100*3,
    marginTop:dim.height/100*0.7,
    marginBottom:0
  }
 
});
