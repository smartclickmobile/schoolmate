import React, {Component} from 'react';
import {
  Platform, 
  StyleSheet, 
  Text, 
  View,
  Image,
  Dimensions,
  TextInput,
  TouchableOpacity,
  BackHandler,
  ToastAndroid,
  Alert
} from 'react-native';
import {MainColor,SubColor, MainColor1} from '../../Assets/color'
import DeviceInfo from "react-native-device-info"
// import { Input } from 'native-base';
import Icons from "react-native-vector-icons/FontAwesome"
import { Actions } from 'react-native-router-flux';
import * as actions from "../Actions"
import {connect} from "react-redux"
import {
  SIZE_4_5,
  SIZE_9
} from "../Constant/font"
class Login extends Component {
    constructor(props){
        super(props)
        this.state={
            logo:true,
            username:'',
            password:'',
            checkX:'nox'
        }
    }
    componentDidMount(){
        BackHandler.addEventListener("back", () => this._handleBack())
    }
    componentWillReceiveProps(nextProps){
      console.warn('nextProps',nextProps.error)
   
    }
    _handleBack() {
        if(Actions.currentScene == "_login" || Actions.currentScene == 'login') {
            BackHandler.exitApp()
          return true
        }
        return false
      }
    onlogin(){
      // console.warn('onlogin',this.state.username != '' && this.state.password != '')
      if(this.state.username != '' && this.state.password != ''){
        var data = {
          username:this.state.username,
          password:this.state.password
        }
        this.props.Loginuser(data)
      }else{
        Alert.alert('login fail','กรุณากรอก username และ password', [{ text: 'OK', onPress: null }])
      }
     
    }
  render() {
    return (
      <View style={styles.container}>
        <Image
        source={require('../../Assets/Images/Group.png')}
        style={{
            width:Dimensions.get('window').width,
            height:Dimensions.get('window').height/100*30,
            alignSelf:'center',
            position:'absolute',
        }}
        resizeMode={'stretch'}
        />
        <Text style={[styles.title,{marginTop:this.props.checkIpnoe == 'nox'?Dimensions.get('window').height/100*3:Dimensions.get('window').height/100*10}]}>Login</Text>
        <View style={styles.containerinput}>
            <Text style={styles.text}>Username</Text>
            <TextInput  
              underlineColorAndroid="transparent" 
              style={styles.TextInput}
              onChangeText={(value)=>{this.setState({username:value})}}
              onSubmitEditing={()=>this.password.focus()}
            />
            <Text style={styles.text}>Password</Text>
            <TextInput  
              ref={(input) => { this.password = input; }} 
              underlineColorAndroid="transparent" 
              style={styles.TextInput}
              onChangeText={(value)=>{this.setState({password:value})}}
              onSubmitEditing={()=> {
                this.onlogin()
              }}
              secureTextEntry={true}
            />
            <TouchableOpacity style={styles.bottonlogin} onPress={()=> this.onlogin()}>
              <Text style={[styles.title,{marginTop:0,fontSize:SIZE_4_5}]}>Log in</Text>
            </TouchableOpacity>
            
        </View>
        <View style={{height:Dimensions.get('window').height/100*12,justifyContent:'flex-end'}}>
          <Text style={{textAlign:'right'}}>ver.{DeviceInfo.getVersion()}</Text>
        </View>
        
      </View>
    );
  }
}
const mapStateToProps = state => {
  return {
      success: state.Register.RegisterSuccess,
      error: state.Register.error,
      checkIpnoe:state.check.checkIpnoe,
  }
}
export default connect(mapStateToProps, actions)(Login)
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor:'#fff'
  },
  title:{
    fontFamily:'Kanit-Regular',
    color:'#fff',
    textAlign:'center',
    fontSize:SIZE_9
  },
  containerinput: {
      width:Platform.isPad?Dimensions.get('window').width/100*60:Dimensions.get('window').width/100*90,
      height:Dimensions.get('window').height/100*70,
      backgroundColor:'#fff',
      alignSelf:'center',
      marginTop:Dimensions.get('window').height/100*3,
      borderWidth: 1,
      borderRadius: Dimensions.get('window').width/100*3,
      borderColor: '#ddd',
      borderBottomWidth: 0,
      shadowColor: '#000',
      shadowOffset: { width: 0, height: 2 },
      shadowOpacity: 0.8,
      shadowRadius: 2,
      elevation: 5,
      padding:20
    },
    text:{
      fontFamily:'Kanit-Regular',
      color:'#333',
      fontSize:SIZE_4_5,
      marginTop:Dimensions.get('window').height/100*2
    },
    TextInput:{
      borderWidth:1,
      borderColor:'#333',
      marginTop:Dimensions.get('window').height/100*2,
      height:Dimensions.get('window').height/100*6,
      borderRadius: Dimensions.get('window').width/100*0.5,
      fontFamily:'Kanit-Regular',
      fontSize:SIZE_4_5,
    },
    text1:{
      textAlign:'center',
      fontSize:SIZE_4_5,
      color:'#333',
      fontFamily:'Kanit-Regular',
      marginTop:Dimensions.get('window').height/100*3
    },
    bottonlogin:{
      backgroundColor:'#908CE5',
      height:Dimensions.get('window').height/100*7,
      marginTop:Dimensions.get('window').height/100*12,
      borderRadius:Dimensions.get('window').width/100*3,
      alignItems:'center',
      justifyContent:'center',
      elevation: 5
    }
    
 
});