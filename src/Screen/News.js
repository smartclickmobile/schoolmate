
import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,Dimensions,TouchableOpacity,ScrollView,ActivityIndicator} from 'react-native';
import {  Thumbnail } from 'native-base';
import { MainColor } from '../../Assets/color';
import { Actions } from 'react-native-router-flux';
import * as actions from "../Actions"
import {connect} from "react-redux"
import moment from "moment"
import { SIZE_4 } from '../Constant/font'
class News extends Component {
    constructor(props){
        super(props)
        this.state = {
            data: [],
            loading:false
        }
    }
    componentDidMount(){
        this.props.getNews()
    }
    componentWillReceiveProps(nextProps){
        // console.warn('news',nextProps.NewsSuccess)
        if(nextProps.NewsSuccess != null && nextProps.NewsSuccess != undefined){
            this.setState({data:nextProps.NewsSuccess})
        }
        if(nextProps.loading != null && nextProps.loading != undefined){
            this.setState({loading:nextProps.loading})
        }
    }
    renderdata(){
        return(
            <View>
            {
                this.state.data.map((item,index)=>{
                    return(
                      <TouchableOpacity key={index} style={{ flexDirection:'row',margin:Dimensions.get('window').width/100*2.5,padding:Dimensions.get('window').width/100*2.5,borderColor:MainColor,borderWidth:0.5,borderRadius:Dimensions.get('window').width/100*2.5,backgroundColor:'#fff'}}
                          onPress={()=>Actions.newsdetail({data:item.education_school_feed_id})}
                      >
                          <View style={{flex:1,marginLeft:Dimensions.get('window').width/100*2.5}}>
                              <Text style={{fontFamily:'Kanit-Bold',fontSize:SIZE_4}}>{item.title+' วันที่ '+moment(item.published_to).format('DD-MM-YYYY')}</Text>
                              <Text style={{fontFamily:'Kanit-Regular',fontSize:SIZE_4}}>{item.short_description}</Text>
                          </View>
                      </TouchableOpacity>
                    )
                })
            }
            </View>
        )
    }
  render() {
    return (
      <ScrollView style={styles.container}>
      {this.state.loading? <ActivityIndicator size="large" color={MainColor} style={{alignItems:'center',marginTop:20}} />:<View/>}
      {!this.state.loading && this.state.data.length != 0?this.renderdata():<View/>}
      {!this.state.loading && this.state.data.length == 0?
        <View style={{ flexDirection:'row',margin:Dimensions.get('window').width/100*2.5,padding:Dimensions.get('window').width/100*2.5,borderColor:MainColor,borderWidth:0.5,borderRadius:Dimensions.get('window').width/100*2.5,backgroundColor:'#fff',justifyContent:'center'}}>
        <Text style={{fontFamily:'Kanit-Bold',fontSize:SIZE_4,textAlign:'center',margin:Dimensions.get('window').height/100*2.5}}>ไม่มีรายการประชาสัมพันธ์</Text>
        </View>
        :
        <View/>}
      </ScrollView>
    );
  }
}
const mapStateToProps = state => {
    return {
        NewsSuccess: state.News.NewsSuccess,
        loading: state.News.loading,
        // error: state.Register.error
    }
  }
  export default connect(mapStateToProps, actions)(News)
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5FCFF',
  },
});
