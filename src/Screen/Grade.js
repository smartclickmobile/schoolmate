
import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,Image,Dimensions,TouchableOpacity,ScrollView,Modal} from 'react-native';
import {  DeckSwiper, Thumbnail } from 'native-base';
import Icons from "react-native-vector-icons/FontAwesome"
import { Icon } from 'react-native-elements'
import { MenuColor_Text , MainColor} from '../../Assets/color'
import { Table, Row, Rows } from 'react-native-table-component';
import * as actions from "../Actions"
import {connect} from "react-redux"
class Grade extends Component {
    constructor(props){
        super(props)
        this.state = {
            data:[],
            tableHead: ['ชื่อวิชา', 'คะแนนรวม', 'เกรด'],
            tableData: [],
            modalchange:false,
            studentlist:[],
            summary:[]
        }
    }
    componentDidMount(){
      this.props.getGrade()
        if(this.props.StudentSuccess != null && this.props.StudentSuccess != undefined){
          var data = []
          data.push(this.props.StudentSuccess)
          this.setState({studentlist:data})
        }
        
      }
      componentWillReceiveProps(nextProps){
        if(nextProps.GradeSuccess.detail != undefined){
          var data = []
          // console.warn(nextProps.GradeSuccess.detail.length)
          var datadata = nextProps.GradeSuccess.detail
          for(var i = 0;i<datadata.length; i++){
            
            var value = {
              subject:datadata[i].name,
              summary_semester_1:datadata[i].assessment_grade.summary_semester_1,
              summary_semester_2:datadata[i].assessment_grade.summary_semester_2,
              total_semester_1:datadata[i].assessment_grade.total_semester_1,
              total_semester_2:datadata[i].assessment_grade.total_semester_2
            }
            data.push(value)
          }
          this.setState({tableData:data})
        }
        if(nextProps.GradeSuccess.summary != undefined){
          var temp = []
          temp.push(nextProps.GradeSuccess.summary)
          this.setState({summary:temp})
        }
        
        // console.warn(nextProps.GradeSuccess.summary)
      }
  render() {
    // console.warn('grade',this.state.summary[0])
    return (
      <View style={styles.container}>
        <Modal visible={this.state.modalchange} transparent={true}
          onRequestClose={() => {
            alert('Modal has been closed.');
          }}>
          <View  style={{
            width: "100%",
            height: "100%",
            position: "absolute",
            backgroundColor:'black',
            opacity:0.7
          }}/>
            <View style={{flex:1,justifyContent:'center',flexDirection:'column',alignItems:'center'}}>
              <View style={{backgroundColor:'#fff',width:Dimensions.get('window').width/100*80,borderRadius:Dimensions.get('window').width/100*3}}>
                <View style={{margin:Dimensions.get('window').width/100*3}}>
                  <View style={{height:Dimensions.get('window').height/100*5.5,borderBottomColor:'#69BBF2',borderBottomWidth:1}}>
                    <Text style={{color:'#1C6BA0',fontSize:Dimensions.get('window').width/100*5.5,fontFamily:'Kanit-Bold'}}>เลือกนักเรียน</Text>
                  </View>
                  {
                    this.state.studentlist.map((item,index)=>{
                      return(
                        <TouchableOpacity key={index} style={{borderBottomColor:'#69BBF2',borderBottomWidth:1,padding:10,flexDirection:'row',alignItems:'center'}} onPress={()=> this.setState({modalchange:!this.state.modalchange})}>
                          <Image
                            source={require('../../Assets/Images/student.png')}
                            style={{
                              height:Platform.isPad?Dimensions.get('window').width/100*12:Dimensions.get('window').width/100*15,
                              width:Platform.isPad?Dimensions.get('window').width/100*12:Dimensions.get('window').width/100*15,
                              borderRadius:Platform.isPad?Dimensions.get('window').width/100*6:Dimensions.get('window').width/100*7.5,
                              borderColor:MainColor,
                              borderWidth:Dimensions.get('window').width/100*1,
                            }}
                            resizeMode={'cover'}
                          />
                          <Text style={[styles.name,{flex:1, marginTop:0,marginLeft:10}]}>ด.ช.จำใจ มากจะจำ</Text>
                        </TouchableOpacity>
                      )
                    })
                  }
                </View>
              </View>
            </View>
          </Modal>
        {this.state.studentlist.length == 1 ? <View style={{padding:Platform.isPad?50: 30,marginTop:Platform.isPad?50: 30}}/>:<TouchableOpacity onPress={()=> this.setState({modalchange:!this.state.modalchange})}>
            <Icon name="exchange" color={MainColor} size={28} type='font-awesome' containerStyle={{width:'100%',alignItems:'flex-end',padding:30}}/>
        </TouchableOpacity>}
        <Image
            source={this.props.dataprops.image==null?require('../../Assets/Images/user.png'):{uri:this.props.dataprops.image}}
            style={{
                height:Platform.isPad?Dimensions.get('window').width/100*30:Dimensions.get('window').width/100*45,
                width:Platform.isPad?Dimensions.get('window').width/100*30:Dimensions.get('window').width/100*45,
                borderRadius:Platform.isPad?Dimensions.get('window').width/100*15:Dimensions.get('window').width/100*22.5,
                borderColor:MainColor,
                borderWidth:Dimensions.get('window').width/100*1,
                alignSelf:'center',
                marginTop:Platform.isPad?-(Dimensions.get('window').height/100*10):-(Dimensions.get('window').height/100*7)
            }}
            // resizeMode={'cover'}
            />
        <Text style={[styles.name]}>{this.props.dataprops.title+' '+this.props.dataprops.thai_firstname+' '+this.props.dataprops.thai_lastname}</Text>
        {/*<View style={{flexDirection:'row'}}>
            <TouchableOpacity style={{flex:1,flexDirection:'row',margin:Dimensions.get('window').height/100*1.5,borderColor:MainColor,borderWidth:1,borderRadius:Dimensions.get('window').height/100*1}}>
                <Text style={{flex:3,textAlign:'center',fontSize:Dimensions.get('window').height/100*2.5,fontFamily:'Kanit-Bold',color:MainColor}}>ปี 2561</Text>
                <Icons name="sort-down" color={MainColor} size={22} style={{flex:1}}/>
            </TouchableOpacity>
            <TouchableOpacity style={{flex:1,flexDirection:'row',margin:Dimensions.get('window').height/100*1.5,borderColor:MainColor,borderWidth:1,borderRadius:Dimensions.get('window').height/100*1}}>
                <Text style={{flex:3,textAlign:'center',fontSize:Dimensions.get('window').height/100*2.5,fontFamily:'Kanit-Bold',color:MainColor}}>เทอม 1</Text>
                <Icons name="sort-down" color={MainColor} size={22} style={{flex:1}}/>
            </TouchableOpacity>
          </View>*/}
        <ScrollView style={{padding:5}}>
            {/*<Table borderStyle={{borderWidth: 2, borderColor: '#c8e1ff'}}>
                <Row data={this.state.tableHead} style={styles.head} textStyle={[styles.text]}/>
                <Rows data={this.state.tableData} textStyle={styles.text}/>
            </Table>*/}
            <View style={{marginBottom:20,backgroundColor:'#fff'}}>
                <View style={{flexDirection:'row',backgroundColor:'#AEDEFF'}}>
                    {
                        this.state.tableHead.map((item,index) => {
                            return(
                                <View key={index} style={{flex:1,height:Dimensions.get('window').height/100*6,justifyContent:'center'}}>
                                    <Text style={styles.detailtable}>{item}</Text>
                                </View>
                            )
                        })
                    }
                    
                </View>
                <View>
                    {
                        this.state.tableData.map((item,index) => {
                          // console.warn('item',item)
                            return(
                                <View key={index} style={{flexDirection:'row',borderBottomColor:'#AEDEFF',borderBottomWidth:0.5}}>
                                    
                                    <View key={index} style={{flex:1,height:Dimensions.get('window').height/100*6,justifyContent:'center'}}>
                                        <Text style={styles.detailtable}>{item.subject}</Text>
                                    </View>
                                    <View key={index} style={{flex:1,height:Dimensions.get('window').height/100*6,justifyContent:'center'}}>
                                        <Text style={styles.detailtable}>{this.props.semester==1?item.total_semester_1:item.total_semester_2}</Text>
                                    </View>
                                    <View key={index} style={{flex:1,height:Dimensions.get('window').height/100*6,justifyContent:'center'}}>
                                        <Text style={styles.detailtable}>{this.props.semester==1?item.summary_semester_1:item.summary_semester_2}</Text>
                                    </View>
                                </View>
                                
                            )
                        })
                    }
                </View>
            </View>
        </ScrollView>
        
        <View>
            <View style={{flexDirection:'row',backgroundColor:MainColor}}>
                <Text style={{flex:2,marginLeft:Dimensions.get('window').width/100*3,fontSize:Dimensions.get('window').height/100*3,fontFamily:'Kanit-Regular',color:'#fff'}}>เกรดเฉลี่ยรวม</Text>
                <Text style={{flex:1, textAlign:'right',marginRight:Dimensions.get('window').width/100*3,fontSize:Dimensions.get('window').height/100*3,fontFamily:'Kanit-Regular',color:'#fff'}}>{this.state.summary[0]!=undefined?this.state.summary[0].grade_snummary:''}</Text>
            </View>
            {/*<View style={{flexDirection:'row',backgroundColor:MainColor}}>
                <Text style={{flex:2,marginLeft:Dimensions.get('window').width/100*3,fontSize:Dimensions.get('window').height/100*3,fontFamily:'Kanit-Regular',color:'#fff'}}>เกรดเฉลี่ยปีการศึกษา</Text>
                <Text style={{flex:1, textAlign:'right',marginRight:Dimensions.get('window').width/100*3,fontSize:Dimensions.get('window').height/100*3,fontFamily:'Kanit-Regular',color:'#fff'}}>3.45</Text>
                  </View>*/}
        </View>
      </View>
    );
  }
}
const mapStateToProps = state => {
    return {
      StudentSuccess: state.student.StudentSuccess,
      studenterror: state.student.error,
      EnrollSuccess: state.student.EnrollSuccess,
      GradeSuccess: state.student.grade,
    }
  }
  export default connect(mapStateToProps, actions)(Grade)
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5FCFF',
  },
  hText:{
    fontFamily:'Kanit-Bold',
    color:'#fff',
    fontSize:Dimensions.get('window').height/100*2.5,
    marginLeft:Dimensions.get('window').width/100*3,
    marginTop:Dimensions.get('window').height/100*1.7,
  },
  head: { 
    height: Dimensions.get('window').height/100*8, 
    backgroundColor: '#f1f8ff' ,
    // fontFamily:'Kanit-Bold',
    alignSelf:'center'
},
  text: { 
    margin: 6 ,
    // fontFamily:'Kanit-Regular'
},
name:{
    color:'#333',
    fontFamily:'Kanit-Regular',
    fontSize:Dimensions.get('window').width/100*6,
    alignSelf:'center',
    marginTop:Dimensions.get('window').height/100*1.5
  },
  containerinput: {
    width:Dimensions.get('window').width/100*93,
    // height:Dimensions.get('window').height/100*20,
    backgroundColor:'#fff',
    alignSelf:'center',
    marginTop:Dimensions.get('window').height/100*3,
    borderWidth: 1,
    borderRadius: Dimensions.get('window').width/100*3,
    borderColor: '#ddd',
    borderBottomWidth: 0,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 2,
    padding:10
  },
  containergrade: {
    width:Dimensions.get('window').width/100*93,
    // height:Dimensions.get('window').height/100*20,
    backgroundColor:'#AEDEFF',
    alignSelf:'center',
    marginTop:Dimensions.get('window').height/100*3,
    // borderWidth: 1,
    borderRadius: Dimensions.get('window').width/100*3,
    // borderColor: '#ddd',
    // borderBottomWidth: 0,
    // shadowColor: '#000',
    // shadowOffset: { width: 0, height: 2 },
    // shadowOpacity: 0.8,
    // shadowRadius: 2,
    // elevation: 2,
    padding:10,
    marginBottom:Dimensions.get('window').height/100*3
  },
  h1:{
    color:'#1C6BA0',
    fontFamily:'Kanit-Regular',
    fontSize:Dimensions.get('window').width/100*4
  },
  text:{
    color:'#333',
    fontFamily:'Kanit-Regular',
    fontSize:Dimensions.get('window').width/100*4
  },
  botton:{
    backgroundColor:'#908CE5',
    height:Dimensions.get('window').height/100*6,
    borderRadius:Dimensions.get('window').width/100*3,
    alignItems:'center',
    justifyContent:'center',
    flex:1
  },
  detailtable:{
    textAlign:'center',
    fontFamily:'Kanit-Regular',
    fontSize:Dimensions.get('window').width/100*4
    }
  
});
