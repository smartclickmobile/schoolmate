import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,Dimensions,TouchableOpacity,ScrollView,Image,Modal} from 'react-native';
import {  Thumbnail, Container, Content } from 'native-base';
import { MainColor } from '../../Assets/color';
import { Actions } from 'react-native-router-flux';
import { Icon } from 'react-native-elements'
import * as actions from "../Actions"
import {connect} from "react-redux"
class InfoStudent extends Component {
    constructor(props){
        super(props)
        this.state = {
            data: [],
            modalchange:false,
            studentlist:[{no:1},{no:1}],
            summary:[]
        }
    }
    componentDidMount(){
      // console.warn(this.props.StudentSuccess)
      if(this.props.StudentSuccess != null && this.props.StudentSuccess != undefined){
        var data = []
        data.push(this.props.StudentSuccess)
        this.setState({data:data,studentlist:data})
      }
      this.props.getGrade()
    }
    componentWillReceiveProps(nextProps){
      // console.warn(nextProps.GradeSuccess)
      if(nextProps.GradeSuccess.summary != undefined){
        var temp = []
        temp.push(nextProps.GradeSuccess.summary)
        this.setState({summary:temp})
      }
    }
  render() {
   
    return (
      <Container style={styles.container}>
        <Modal visible={this.state.modalchange} transparent={true}
          onRequestClose={() => {
            alert('Modal has been closed.');
          }}>
          <View  style={{
            width: "100%",
            height: "100%",
            position: "absolute",
            backgroundColor:'black',
            opacity:0.7
          }}/>
            <View style={{flex:1,justifyContent:'center',flexDirection:'column',alignItems:'center'}}>
              <View style={{backgroundColor:'#fff',width:Dimensions.get('window').width/100*80,borderRadius:Dimensions.get('window').width/100*3}}>
                <View style={{margin:Dimensions.get('window').width/100*3}}>
                  <View style={{height:Dimensions.get('window').height/100*5.5,borderBottomColor:'#69BBF2',borderBottomWidth:1}}>
                    <Text style={{color:'#1C6BA0',fontSize:Dimensions.get('window').width/100*5.5,fontFamily:'Kanit-Bold'}}>เลือกนักเรียน</Text>
                  </View>
                  {
                    this.state.studentlist.map((item,index)=>{
                      return(
                        <TouchableOpacity key={index} style={{borderBottomColor:'#69BBF2',borderBottomWidth:1,padding:10,flexDirection:'row',alignItems:'center'}} onPress={()=> this.setState({modalchange:!this.state.modalchange})}>
                          <Image
                            source={require('../../Assets/Images/student.png')}
                            style={{
                              height:Dimensions.get('window').width/100*15,
                              width:Dimensions.get('window').width/100*15,
                              borderRadius:Dimensions.get('window').width/100*7.5,
                              borderColor:MainColor,
                              borderWidth:Dimensions.get('window').width/100*1,
                            }}
                            resizeMode={'cover'}
                          />
                          <Text style={[styles.name,{flex:1, marginTop:0,marginLeft:10}]}>ด.ช.จำใจ มากจะจำ</Text>
                        </TouchableOpacity>
                      )
                    })
                  }
                </View>
              </View>
            </View>
          </Modal>
        <Content>
            {this.state.studentlist.length == 1 ?<View style={{padding:Platform.isPad?50: 30}}/>:<TouchableOpacity 
            onPress={()=> this.setState({modalchange:!this.state.modalchange})}
            >
              <Icon name="exchange" color={MainColor} size={28} type='font-awesome' containerStyle={{width:'100%',alignItems:'flex-end',padding:30}}/>
            </TouchableOpacity>}
            <Image
                source={this.props.dataprops.image==null?require('../../Assets/Images/user.png'):{uri:this.props.dataprops.image}}
                style={{
                  height:Platform.isPad?Dimensions.get('window').width/100*30: Dimensions.get('window').width/100*40,
                  width:Platform.isPad?Dimensions.get('window').width/100*30:Dimensions.get('window').width/100*40,
                  borderRadius:Platform.isPad?Dimensions.get('window').width/100*15:Dimensions.get('window').width/100*20,
                  borderColor:MainColor,
                  borderWidth:Dimensions.get('window').width/100*1,
                  alignSelf:'center',
                  marginTop:-(Dimensions.get('window').height/100*6),
                }}
                resizeMode={'cover'}
              />
              <Text style={[styles.name]}>{this.props.dataprops.title+' '+this.props.dataprops.thai_firstname+' '+this.props.dataprops.thai_lastname}</Text>
              <View style={styles.containerinput}>
                <View style={{flex:1,paddingLeft:15,justifyContent:'center'}}>
                    
                    <View style={{flexDirection:'row',marginTop:Dimensions.get('window').height/100*0.7}}>
                        <Text style={[styles.h1,{flex:1}]}>เลขประจำตัว</Text>
                        <Text style={[styles.text,{flex:2}]}>{this.props.dataprops.code}</Text>
                    </View>
                    <View style={{flexDirection:'row',marginTop:Dimensions.get('window').height/100*0.7}}>
                        <Text style={[styles.h1,{flex:1}]}>ชั้น</Text>
                        <Text style={[styles.text,{flex:2}]}>{this.props.room}</Text>
                    </View>
                </View>
              </View>
              <View style={styles.containergrade}>
                <View style={{flexDirection:'row',borderBottomColor:'#69BBF2',borderBottomWidth:1,alignItems:'center',padding:10}}>
                    <Text style={[styles.text,{flex:1,fontFamily:'Kanit-Bold',fontSize:Dimensions.get('window').width/100*5.5}]}>เกรด</Text>
                    {/*<TouchableOpacity onPress={()=> Actions.grade()} style={styles.botton} onPress={()=> Actions.infostudent()}>
                        <Text style={[styles.text,{color:'#fff',fontSize:Dimensions.get('window').width/100*5.5}]}>ดูย้อนหลัง</Text>
              </TouchableOpacity>*/}
                </View>
                <View style={{flexDirection:'row',borderBottomColor:'#69BBF2',borderBottomWidth:1,alignItems:'center',padding:10}}>
                    <Text style={[styles.h1,{flex:1,fontSize:Dimensions.get('window').width/100*5}]}>เทอม</Text>
                    <Text style={[styles.text,{flex:1,fontSize:Dimensions.get('window').width/100*5,textAlign:'left'}]}>1</Text>
                    <Text style={[styles.text,{flex:1,fontSize:Dimensions.get('window').width/100*5,color:'red',textAlign:'center'}]}>{this.state.summary[0]!=undefined?this.state.summary[0].grade_snummary_semester_1:''}</Text>
                    <View style={{flex:1,alignItems:'flex-end'}}>
                        <TouchableOpacity onPress={()=> Actions.grade({dataprops:this.props.dataprops,room:this.props.room,semester:1})} style={{width:Dimensions.get('window').width/100*10,height:Dimensions.get('window').width/100*10,backgroundColor:'#1C6BA0',borderRadius:Dimensions.get('window').width/100*5,alignItems:'center',justifyContent:'center'}}>
                            <Icon name={'search'} type='font-awesome' color={'#fff'} size={Platform.isPad?35: 26}/>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{flexDirection:'row',borderBottomColor:'#69BBF2',borderBottomWidth:1,alignItems:'center',padding:10}}>
                    <Text style={[styles.h1,{flex:1,fontSize:Dimensions.get('window').width/100*5}]}>เทอม</Text>
                    <Text style={[styles.text,{flex:1,fontSize:Dimensions.get('window').width/100*5,textAlign:'left'}]}>2</Text>
                    <Text style={[styles.text,{flex:1,fontSize:Dimensions.get('window').width/100*5,color:'red',textAlign:'center'}]}>{this.state.summary[0]!=undefined?this.state.summary[0].grade_snummary_semester_2:''}</Text>
                    <View style={{flex:1,alignItems:'flex-end'}}>
                        <TouchableOpacity onPress={()=> Actions.grade({dataprops:this.props.dataprops,room:this.props.room,semester:2})} style={{width:Dimensions.get('window').width/100*10,height:Dimensions.get('window').width/100*10,backgroundColor:'#1C6BA0',borderRadius:Dimensions.get('window').width/100*5,alignItems:'center',justifyContent:'center'}}>
                            <Icon name={'search'} type='font-awesome' color={'#fff'} size={Platform.isPad?35: 26}/>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{flexDirection:'row',alignItems:'center',padding:10}}>
                    <Text style={[styles.h1,{flex:1,fontSize:Dimensions.get('window').width/100*5}]}></Text>
                    <Text style={[styles.h1,{flex:1,fontSize:Dimensions.get('window').width/100*5,textAlign:'left'}]}>รวม</Text>
                    <Text style={[styles.text,{flex:1,fontSize:Dimensions.get('window').width/100*5,color:'red',textAlign:'center'}]}>{this.state.summary[0]!=undefined?this.state.summary[0].grade_snummary:''}</Text>
                    <View style={{flex:1,alignItems:'flex-end'}}>
                        
                    </View>
                </View>
              </View>
        </Content>
      </Container>
    );
  }
}
const mapStateToProps = state => {
  return {
    StudentSuccess: state.student.StudentSuccess,
    studenterror: state.student.error,
    EnrollSuccess: state.student.EnrollSuccess,
    GradeSuccess: state.student.grade,
  }
}
export default connect(mapStateToProps, actions)(InfoStudent)
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5FCFF',
  },
  name:{
    color:'#333',
    fontFamily:'Kanit-Regular',
    fontSize:Dimensions.get('window').width/100*6,
    alignSelf:'center',
    marginTop:Dimensions.get('window').height/100*3
  },
  containerinput: {
    width:Dimensions.get('window').width/100*93,
    // height:Dimensions.get('window').height/100*20,
    backgroundColor:'#fff',
    alignSelf:'center',
    marginTop:Dimensions.get('window').height/100*3,
    borderWidth: 1,
    borderRadius: Dimensions.get('window').width/100*3,
    borderColor: '#ddd',
    borderBottomWidth: 0,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 2,
    padding:10
  },
  containergrade: {
    width:Dimensions.get('window').width/100*93,
    // height:Dimensions.get('window').height/100*20,
    backgroundColor:'#AEDEFF',
    alignSelf:'center',
    marginTop:Dimensions.get('window').height/100*3,
    // borderWidth: 1,
    borderRadius: Dimensions.get('window').width/100*3,
    // borderColor: '#ddd',
    // borderBottomWidth: 0,
    // shadowColor: '#000',
    // shadowOffset: { width: 0, height: 2 },
    // shadowOpacity: 0.8,
    // shadowRadius: 2,
    // elevation: 2,
    padding:10,
    marginBottom:Dimensions.get('window').height/100*3
  },
  h1:{
    color:'#1C6BA0',
    fontFamily:'Kanit-Regular',
    fontSize:Dimensions.get('window').width/100*4
  },
  text:{
    color:'#333',
    fontFamily:'Kanit-Regular',
    fontSize:Dimensions.get('window').width/100*4
  },
  botton:{
    backgroundColor:'#908CE5',
    height:Dimensions.get('window').height/100*6,
    borderRadius:Dimensions.get('window').width/100*3,
    alignItems:'center',
    justifyContent:'center',
    flex:1
  }
});
