
import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,Dimensions,Image,TouchableOpacity, TextInput, ScrollView, Modal,Keyboard,DatePickerIOS} from 'react-native';
import {MainColor} from '../../Assets/color'
import { Actions } from 'react-native-router-flux';
import DateTimePicker from "react-native-modal-datetime-picker"
import moment from "moment"
import {connect} from "react-redux"
import * as actions from "../Actions"
import { Container, Content } from 'native-base';
import {CheckBox} from 'react-native-elements'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
class Register extends Component {
  constructor(props){
    super(props)
    this.state = {
      parent_firstname:'',
      parent_lastname:'',
      parent_citizen_number:'',
      parent_tel:'',
      parent_address:'',
      parent_password:'',
      parent_retype_password:'',
      parent_email:'',
      parent_username:'',

      student_firstname:'',
      student_lastname:'',
      student_citizen_number:'',
      student_birth_date:'',

      Cparent_firstname:true,
      Cparent_lastname:true,
      Cparent_citizen_number:true,
      Cparent_tel:true,
      Cparent_address:true,
      Cparent_email:true,
      Cparent_password:true,
      Cparent_retype_password:true,
      Cparent_username:true,

      Cstudent_firstname:true,
      Cstudent_lastname:true,
      Cstudent_citizen_number:true,
      Cstudent_birth_date:true,
      Cimage:true,

      datevisibledate:false,
      datevisibledate1:false,
      modalVisible:false,
      errorText:'',

      checked:false
    }
  }

  checkok(){
    if(this.state.checked){
      this.onConfirmregister()
    }else{
      this.setState({errorText:'กรุณากดยอมรับเงือนไข'},()=>{
        this.setState({modalVisible:true})
      })

    }
  }

  onConfirmregister(){
    console.warn(this.state.student_firstname != '',this.state.student_lastname != '',this.state.student_citizen_number != '',this.state.student_birth_date != '')
    if(
      this.state.parent_firstname != '' &&
      this.state.parent_lastname != '' &&
      this.state.parent_citizen_number != '' &&
      this.state.parent_tel != '' &&
      this.state.parent_address != ''
    ){
      if(
        this.state.student_firstname != '' &&
        this.state.student_lastname != '' &&
        this.state.student_citizen_number != '' &&
        this.state.student_birth_date != ''
      ){
        console.warn('gg')
        if(
          this.state.parent_password == this.state.parent_retype_password
        ){
          var data = {
            parent_username: this.state.parent_username,
            parent_firstname:this.state.parent_firstname,
            parent_lastname:this.state.parent_lastname,
            parent_citizen_number:this.state.parent_citizen_number,
            parent_tel:this.state.parent_tel,
            parent_address:this.state.parent_address,
            parent_password:this.state.parent_password,
            parent_retype_password:this.state.parent_retype_password,
      
            student_firstname:this.state.student_firstname,
            student_lastname:this.state.student_lastname,
            student_citizen_number:this.state.student_citizen_number,
            student_birth_date:this.state.student_birth_date,
            // image:this.state.image,
          }
          this.props.registeruser(data)

        }
        else{
          this.setState({errorText:'รหัสผ่านไม่ถูกต้อง'},()=>{
            this.setState({modalVisible:true})
          })
        }
      }else{
        this.setState({errorText:'กรุณากรอกข้อมูลนักเรียนให้ครบถ้วน'},()=>{
          this.setState({modalVisible:true})
        })
      }
    }else{
      this.setState({errorText:'กรุณากรอกข้อมูลผู้ปกครองให้ครบถ้วน'},()=>{
        this.setState({modalVisible:true})
      })
    }
    
    
  
  }
  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }
  render() {
    var showDatePicker = this.state.datevisibledate1 ?
    <DatePickerIOS
        style={{ height: 150 }}
        date={this.state.student_birth_date} onDateChange={(date)=>this.setState({student_birth_date:date})}
        mode="date"/> : <View />
    return (
      <Container style={styles.container}>
        {showDatePicker}
        <Modal visible={this.state.modalVisible} transparent={true}
          onRequestClose={() => {
            alert('Modal has been closed.');
          }}
        >
          <View  style={{
            width: "100%",
            height: "100%",
            position: "absolute",
            backgroundColor:'black',
            opacity:0.7
          }}/>
          <View style={{flex:1,justifyContent:'center',flexDirection:'column',alignItems:'center'}}>
            <View style={{backgroundColor:'#fff',width: Dimensions.get('window').width/100*80,borderRadius: Dimensions.get('window').width/100*3}}>
              <View style={{margin: Dimensions.get('window').width/100*3}}>
                <View style={{height: Dimensions.get('window').height/100*6,borderBottomColor:'#908CE5',borderBottomWidth: Dimensions.get('window').width/100*0.2}}>
                  <Text style={{color:'#908CE5',fontSize: Dimensions.get('window').width/100*4.5,fontFamily:'Kanit-Bold'}}>กรอกข้อมูลไม่ครบถ้วน</Text>
                </View>
                <Text style={{color:'black',fontSize: Dimensions.get('window').width/100*4,fontFamily:'Kanit-Regular',marginVertical:Dimensions.get('window').height/100*2}}>{this.state.errorText}</Text>
                <View style={{flexDirection:'row',height: Dimensions.get('window').height/100*10}}>
                  <TouchableOpacity style={{flex:1,margin: Dimensions.get('window').width/100*3,backgroundColor:'#ecf0f1',justifyContent:'center',borderRadius: Dimensions.get('window').width/100*2}} onPress={() => {this.setModalVisible(!this.state.modalVisible);}}>
                    <Text style={{color:'black',fontSize: Dimensions.get('window').width/100*4,textAlign:'center',fontFamily:'Kanit-Regular'}}>Closed</Text>
                  </TouchableOpacity>
                    </View>
                  </View>
              </View>
          </View>
        </Modal>
        <DateTimePicker
          isVisible={this.state.datevisibledate}
          onConfirm={(date) => { 
            this.setState({ datevisibledate: false },
              this.setState({student_birth_date:moment(date).format("DD/MM/YYYY")},()=>{
                console.warn('date',typeof this.state.student_birth_date)
              })
            )}}
          onCancel={() => this.setState({ datevisibledate: false })}
          datePickerModeAndroid="spinner"
          mode='date'
              />

      <KeyboardAwareScrollView>
        <Image
        source={require('../../Assets/Images/Group.png')}
        style={{
            width:Dimensions.get('window').width,
            alignSelf:'center',
            position:'absolute',
        }}
        resizeMode={'cover'}
        />
        
        <Text style={styles.title}>Sign Up</Text>
        
          <View style={styles.containerinput}>

            <Text style={styles.h1}>ข้อมูลทั่วไป</Text>
            <Text style={styles.text}>ชื่อ</Text>
            <TextInput 
              ref={(input) => { this.parent_firstname_text = input; }} 
              underlineColorAndroid="transparent" 
              style={this.state.Cparent_firstname==true?styles.TextInput:styles.TextInput_styles_fail}
              onChangeText={(value)=> this.setState({parent_firstname:value})}
              onSubmitEditing={()=> this.parent_lastname_text.focus()}
            />
            <Text style={styles.text}>นามสกุล</Text>
            <TextInput 
              ref={(input) => { this.parent_lastname_text = input; }} 
              underlineColorAndroid="transparent" 
              style={this.state.Cparent_lastname==true?styles.TextInput:styles.TextInput_styles_fail}
              onChangeText={(value)=> this.setState({parent_lastname:value})}
              onSubmitEditing={()=> this.parent_citizen_number_text.focus()}
            />
            {/*<Text style={styles.text}>อีเมล</Text>
            <TextInput 
              ref={(input) => { this.parent_email_text = input; }}  
              underlineColorAndroid="transparent" 
              style={this.state.Cparent_email==true?styles.TextInput:styles.TextInput_styles_fail}
              onChangeText={(value)=> this.setState({parent_email:value})}
              onSubmitEditing={()=> this.parent_tel_text.focus()}
            />*/}
            <Text style={styles.text}>เลขบัตรประชาชน</Text>
            <TextInput 
              ref={(input) => { this.parent_citizen_number_text = input; }} 
              underlineColorAndroid="transparent" 
              style={this.state.Cparent_citizen_number==true?styles.TextInput:styles.TextInput_styles_fail}
              onChangeText={(value)=> this.setState({parent_citizen_number:value})}
              keyboardType='numeric'
              onSubmitEditing={()=> this.parent_address_text.focus()}
            />
            <Text style={styles.text}>ที่อยู่</Text>
            <TextInput 
              ref={(input) => { this.parent_address_text = input; }} 
              underlineColorAndroid="transparent" 
              style={this.state.Cparent_address==true?styles.TextInput:styles.TextInput_styles_fail}
              onChangeText={(value)=> this.setState({parent_address:value})}
              onSubmitEditing={()=> this.parent_tel_text.focus()}
            />
            <Text style={styles.text}>เบอร์โทรติดต่อ</Text>
            <TextInput 
              ref={(input) => { this.parent_tel_text = input; }} 
              underlineColorAndroid="transparent" 
              style={this.state.Cparent_tel==true?styles.TextInput:styles.TextInput_styles_fail}
              onChangeText={(value)=> this.setState({parent_tel:value})}
              keyboardType='phone-pad'
              onSubmitEditing={()=> this.student_firstname_text.focus()}
            />
           

            <Text style={styles.h1}>ข้อมูลนักเรียน</Text>
            <Text style={styles.text}>ชื่อ</Text>
            <TextInput 
              ref={(input) => { this.student_firstname_text = input; }} 
              underlineColorAndroid="transparent" 
              style={this.state.Cstudent_firstname==true?styles.TextInput:styles.TextInput_styles_fail}
              onChangeText={(value)=> this.setState({student_firstname:value})}
              onSubmitEditing={()=> this.student_lastname_text.focus()}
            />
            <Text style={styles.text}>นามสกุล</Text>
            <TextInput 
              ref={(input) => { this.student_lastname_text = input; }} 
              underlineColorAndroid="transparent" 
              style={this.state.Cstudent_lastname==true?styles.TextInput:styles.TextInput_styles_fail}
              onChangeText={(value)=> this.setState({student_lastname:value})}
              onSubmitEditing={()=> this.student_citizen_number_text.focus()}
            />
            <Text style={styles.text}>เลขบัตรประชาชน</Text>
            <TextInput 
              ref={(input) => { this.student_citizen_number_text = input; }} 
              underlineColorAndroid="transparent" 
              style={this.state.Cstudent_citizen_number==true?styles.TextInput:styles.TextInput_styles_fail}
              onChangeText={(value)=> this.setState({student_citizen_number:value})}
              keyboardType='numeric'
              onSubmitEditing={(value)=> this.setState({student_citizen_number:value})}
            />
            <Text  style={styles.text}>วันเกิด</Text>
            <TouchableOpacity onPress={()=> {
              console.warn('date ios')
              // if(Platform.OS == 'ios'){
              //   console.warn('press date ios')
              //   this.setState({ datevisibledate1: true })
              // }else{
                this.setState({ datevisibledate: true })
              // }
              
            }}
            style={this.state.Cstudent_birth_date==true?styles.TextInput:styles.TextInput_styles_fail}
            >
             {/* <TextInput 
                underlineColorAndroid="transparent" 
                style={this.state.Cstudent_birth_date==true?styles.TextInput:styles.TextInput_styles_fail}
                value={this.state.student_birth_date}
                editable={false}
             />*/}
            </TouchableOpacity>
            

            <Text style={styles.h1}>ลงทะเบียน</Text>
            <Text style={styles.text}>Username</Text>
            <TextInput 
              ref={(input) => { this.parent_username_text = input; }} 
              underlineColorAndroid="transparent" 
              style={this.state.Cparent_username==true?styles.TextInput:styles.TextInput_styles_fail}
              onChangeText={(value)=> this.setState({parent_username:value})}
              onSubmitEditing={()=> this.parent_password_text.focus()}
            />
            <Text style={styles.text}>Password</Text>
            <TextInput 
              ref={(input) => { this.parent_password_text = input; }} 
              underlineColorAndroid="transparent" 
              style={this.state.Cparent_password==true?styles.TextInput:styles.TextInput_styles_fail}
              onChangeText={(value)=> this.setState({parent_password:value})}
              secureTextEntry={true}
              // onSubmitEditing={()=> this.parent_retype_password_text.focus()}
            />
            <Text style={styles.text}>Re-Password</Text>
            <TextInput 
              ref={(input) => { this.parent_retype_password_text = input; }} 
              underlineColorAndroid="transparent" 
              style={this.state.Cparent_retype_password==true?styles.TextInput:styles.TextInput_styles_fail}
              onChangeText={(value)=> this.setState({parent_retype_password:value})}
              onSubmitEditing={(value)=>  this.setState({parent_retype_password:value})}
              secureTextEntry={true}
              // onSubmitEditing={()=> this.student_firstname_text.focus()}
            />
            <CheckBox
              title='I agree all statement in term of service?'
              checked={this.state.checked}
              onPress={() => this.setState({checked: !this.state.checked})}
              containerStyle={{borderWidth:0,backgroundColor:'#fff',marginTop:Dimensions.get('window').height/100*3}}
              textStyle={{fontFamily:'Kanit-Regular',fontSize:Dimensions.get('window').width/100*3.5}}
            />
            <TouchableOpacity style={styles.bottonlogin} onPress={()=> this.onConfirmregister()}>
              <Text style={[styles.title,{marginTop:0,fontSize:Dimensions.get('window').width/100*4.5}]}>Sign In</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={()=> Actions.pop()}>
              <Text style={styles.text1}>{'< กลับไปยังหน้า Login'}</Text>
            </TouchableOpacity>
          
          
          </View>
        </KeyboardAwareScrollView>
        
        
      </Container>
      
    );
  }
}
const mapStateToProps = state => {
  return {
      success: state.Register.RegisterSuccess,
      error: state.Register.error
  }
}
export default connect(mapStateToProps, actions)(Register)
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  title:{
    fontFamily:'Kanit-Regular',
    color:'#fff',
    textAlign:'center',
    fontSize:Dimensions.get('window').width/100*9,
    marginTop:Dimensions.get('window').height/100*3,
  },
  containerinput: {
      width:Dimensions.get('window').width/100*90,
      // height:Dimensions.get('window').height/100*70,
      backgroundColor:'#fff',
      alignSelf:'center',
      marginTop:Dimensions.get('window').height/100*3,
      borderWidth: 1,
      borderRadius: Dimensions.get('window').width/100*3,
      borderColor: '#ddd',
      borderBottomWidth: 0,
      shadowColor: '#000',
      shadowOffset: { width: 0, height: 2 },
      shadowOpacity: 0.8,
      shadowRadius: 2,
      elevation: 5,
      padding:20,
      marginBottom:Dimensions.get('window').height/100*10
    },
    text:{
      fontFamily:'Kanit-Regular',
      color:'#333',
      fontSize:Dimensions.get('window').width/100*4.5,
      marginTop:Dimensions.get('window').height/100*2
    },
    h1:{
      fontFamily:'Kanit-Bold',
      color:'#333',
      fontSize:Dimensions.get('window').width/100*5,
      marginTop:Dimensions.get('window').height/100*2,
      borderBottomWidth:1,
      padding:Dimensions.get('window').width/100*2,
      borderBottomColor:'#69BBF2'
    },
    TextInput:{
      borderWidth:1,
      borderColor:'#333',
      marginTop:Dimensions.get('window').height/100*2,
      height:Dimensions.get('window').height/100*6,
      borderRadius: Dimensions.get('window').width/100*0.5,
      fontFamily:'Kanit-Regular'
    },
    TextInput_styles_fail:{
      borderWidth:1,
      borderColor:'red',
      marginTop:Dimensions.get('window').height/100*2,
      height:Dimensions.get('window').height/100*6,
      borderRadius: Dimensions.get('window').width/100*0.5,
      fontFamily:'Kanit-Regular'
    },
    text1:{
      textAlign:'center',
      fontSize:Dimensions.get('window').width/100*4.5,
      color:'#333',
      fontFamily:'Kanit-Regular',
      marginTop:Dimensions.get('window').height/100*3,
      marginBottom:Dimensions.get('window').height/100*10
    },
    bottonlogin:{
      backgroundColor:'#908CE5',
      height:Dimensions.get('window').height/100*7,
      marginTop:Dimensions.get('window').height/100*3,
      borderRadius:Dimensions.get('window').width/100*3,
      alignItems:'center',
      justifyContent:'center',
      elevation: 5
    }
 
});
