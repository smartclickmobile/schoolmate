
import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,Dimensions, Image, TouchableOpacity} from 'react-native';
import { MenuColor_Text, MainColor, SidebarColor_Text } from '../../Assets/color'
import Icons from "react-native-vector-icons/FontAwesome"
import { Actions } from 'react-native-router-flux';
import * as actions from "../Actions"
import {connect} from "react-redux"
import { Container, Header, Content, List, ListItem, Left, Body } from 'native-base';
import { SIZE_4_5,SIZE_4,SIZE_2_8,ICON_SIZE } from '../Constant/font'
const dim = Dimensions.get('window');
class SideBar extends Component {
  constructor(props){
    super(props)
    this.state ={
      userinfo:null
    }
  }
  componentWillReceiveProps(nextProps){
    // console.warn('user',nextProps.userinfo)
    if(nextProps.userinfo != null && nextProps.userinfo != undefined){
      var data = []
      data.push(nextProps.userinfo)
      this.setState({userinfo:data})
    }
  }
  componentDidMount(){
    this.props.getuserinfo()
  }
  onlogout(){
    this.props.Logoutuser()
    // Actions.login()
  }
  render() {
    // console.warn('userinfo',this.state.userinfo)
    return (
      <View style={styles.container}>
        <View style={{flex:1}}>
          <View
            style={{
              height:Platform.isPad?dim.height/100*30:dim.height/100*26,
              width:Platform.isPad?Dimensions.get('window').width/100*50:Dimensions.get('window').width/100*70,
              backgroundColor:MainColor,
              justifyContent:'flex-end',
              padding:dim.width/100*3
            }}
          >
            <Image
              source={this.state.userinfo==null?require('../../Assets/Images/user.png'):this.state.userinfo[0].image==null?require('../../Assets/Images/user.png'):{uri:this.state.userinfo[0].image}}
              style={{
                  height:dim.width/100*20,
                  width:dim.width/100*20,
                  borderRadius:dim.width/100*10,
                  borderColor:MainColor,
                  borderWidth:dim.width/100*0.5,
              }}
              resizeMode={'cover'}
            />
            <Text numberOfLines={1} style={{color:MenuColor_Text,fontSize:SIZE_4_5,fontFamily:'Kanit-Bold'}}>{this.state.userinfo==null?'':this.state.userinfo[0].thai_firstname+' '+this.state.userinfo[0].thai_lastname}</Text>
            <Text numberOfLines={1} style={{color:MenuColor_Text,fontSize:SIZE_4,fontFamily:'Kanit-Regular'}}>{this.state.userinfo==null?'':this.state.userinfo[0].citizen_number}</Text>
          </View>
          <View style={{flex:1}}>
            <List style={{flex:1}}>
              <ListItem >
                <TouchableOpacity style={{flexDirection:'row',flex:1}} onPress={()=> Actions.dashborad()}>
                  <Left style={{flex:2}}>
                    <Icons name="home" type='FontAwesome5' color={Actions.currentScene=='_dashborad'?'#4285F4':SidebarColor_Text} size={ICON_SIZE} style={[styles.iconstyle]}/>
                  </Left>
                  <Body style={{flex:8,justifyContent:'center'}}>
                    <Text style={{color:Actions.currentScene=='_dashborad'?'#4285F4':'#333',fontSize:SIZE_4,fontFamily:'Kanit-Regular'}}>หน้าหลัก</Text>
                  </Body>
                </TouchableOpacity>
              </ListItem>

              <ListItem >
                <TouchableOpacity style={{flexDirection:'row',flex:1}} onPress={()=> Actions.calendarscreen()}>
                  <Left style={{flex:2}}>
                    <Icons name="calendar" type='FontAwesome5' color={Actions.currentScene=='_calendarscreen'?'#4285F4':SidebarColor_Text} size={ICON_SIZE} style={[styles.iconstyle]}/>
                  </Left>
                  <Body style={{flex:8,justifyContent:'center'}}>
                    <Text style={{color:Actions.currentScene=='_calendarscreen'?'#4285F4':'#333',fontSize:SIZE_4,fontFamily:'Kanit-Regular'}}>ปฎิทินโรงเรียน</Text>
                  </Body>
                </TouchableOpacity>
              </ListItem>

              <ListItem >
                <TouchableOpacity style={{flexDirection:'row',flex:1}} onPress={()=> Actions.news()}>
                  <Left style={{flex:2}}>
                    <Icons name="align-justify" type='FontAwesome5' color={Actions.currentScene=='_news'?'#4285F4':SidebarColor_Text} size={ICON_SIZE} style={[styles.iconstyle]}/>
                  </Left>
                  <Body style={{flex:8,justifyContent:'center'}}>
                    <Text style={{color:Actions.currentScene=='_news'?'#4285F4':'#333',fontSize:SIZE_4,fontFamily:'Kanit-Regular'}}>ข่าวประชาสัมพันธ์</Text>
                  </Body>
                </TouchableOpacity>
              </ListItem>

              <ListItem >
                <TouchableOpacity style={{flexDirection:'row',flex:1}} onPress={()=> Actions.notification()}>
                  <Left style={{flex:2}}>
                    <Icons name="bell" type='FontAwesome5' color={Actions.currentScene=='_notification'?'#4285F4':SidebarColor_Text} size={ICON_SIZE} style={[styles.iconstyle]}/>
                  </Left>
                  <Body style={{flex:8,justifyContent:'center'}}>
                    <Text style={{color:Actions.currentScene=='_notification'?'#4285F4':'#333',fontSize:SIZE_4,fontFamily:'Kanit-Regular'}}>กล่องแจ้งเตือน</Text>
                  </Body>
                </TouchableOpacity>
              </ListItem>

              <ListItem>
                <TouchableOpacity style={{flexDirection:'row',flex:1}} onPress={()=> this.onlogout()}>
                  <Left style={{flex:2}}>
                    <Icons name="sign-out" type='FontAwesome5' color={SidebarColor_Text} size={ICON_SIZE} style={[styles.iconstyle]}/>
                  </Left>
                  <Body style={{flex:8,justifyContent:'center'}}>
                    <Text style={{color:'#333',fontSize:SIZE_4,fontFamily:'Kanit-Regular'}}>ออกจากระบบ</Text>
                  </Body>
                </TouchableOpacity>
              </ListItem>
           
          </List>
          </View>
        </View>
      </View>
    );
  }
}
const mapStateToProps = state => {
  return {
    userinfo: state.Login.userinfo,
    checkIpnoe:state.check.checkIpnoe,
      // error: state.Register.error
  }
}
export default connect(mapStateToProps, actions)(SideBar)
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5FCFF',
  },
  menucontain: {
    flex:1,
    borderColor:MainColor,
    borderBottomWidth:0.5,
    marginHorizontal:dim.height/100*2.5
  },
  menutext: {
    flex:1,
    color:SidebarColor_Text,
    fontFamily:'Kanit-Bold',
    fontSize:SIZE_2_8,
    textAlign:'center'
  },
  iconstyle:{
    // flex:1,
    // alignSelf:'center',
    // backgroundColor:'red'
    // marginTop:dim.height/100*1.5
  }
});
