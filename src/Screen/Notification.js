
import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,Dimensions,TouchableOpacity,ScrollView} from 'react-native';
import {  Thumbnail } from 'native-base';
import { MainColor } from '../../Assets/color';
import { Actions } from 'react-native-router-flux';
import { SIZE_4 } from '../Constant/font'
export default class Notification extends Component {
    constructor(props){
        super(props)
        this.state = {
            data: [{no:1},{no:1},{no:1},{no:1},{no:1}]
        }
    }
  render() {
    return (
      <ScrollView style={styles.container}>
        <View style={{ flexDirection:'row',margin:Dimensions.get('window').width/100*2.5,padding:Dimensions.get('window').width/100*2.5,borderColor:MainColor,borderWidth:0.5,borderRadius:Dimensions.get('window').width/100*2.5,backgroundColor:'#fff',justifyContent:'center'}}>
          <Text style={{fontFamily:'Kanit-Bold',fontSize:SIZE_4,textAlign:'center',margin:Dimensions.get('window').height/100*2.5}}>ไม่มีรายการแจ้งเตือน</Text>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5FCFF',
  },
});
