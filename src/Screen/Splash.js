import React, { Component } from "react"
import { 
  StyleSheet, 
  View, 
  ActivityIndicator, 
  AsyncStorage, 
  Alert, 
  Linking, 
  Text,
  Image,
  Platform,
  Dimensions 
} from "react-native"
import * as actions from "../Actions"
import { Actions } from "react-native-router-flux"
import { connect } from "react-redux"
import LinearGradient from 'react-native-linear-gradient'
const dim = Dimensions.get('window');
class Splash extends Component {
  constructor(props) {
    super(props)
    
  }
  async componentDidMount() {
    this.props.checkiphonex(dim)
    var time = 0
    var value = await AsyncStorage.getItem('login');
    console.warn('value',value)
      if(value == 'true'){
        setTimeout(() => {
          Actions.dashborad()
        }
        , 1000)
       
      }else{
        setTimeout(() => {
          Actions.login()
        }
        , 1000)
      }
  }

  render() {
    return (
      //'#15E3FF','#47A7EE', '#6C7AE2','#9746D3', '#C212C5'
      <LinearGradient start={{x: 0.0, y: 0.25}} end={{x: 0.5, y: 1.0}} colors={['#ffffff','#ffffff', '#ffffff','#ffffff', '#ffffff']} style={styles.container}>
      <Image
            source={require('../../Assets/Images/bg.png')}
            style={{
                height:Dimensions.get('window').height,
                width:Dimensions.get('window').width,
                alignSelf:'center',
                position:'absolute',
                opacity:0.2
            }}
            resizeMode={'cover'}
            />
        {
            // this.state.logo?
            <Image
            source={require('../../Assets/Images/logo.png')}
            style={{
                height:Dimensions.get('window').height/100*40,
                width:Dimensions.get('window').height/100*40,
                // marginTop:Dimensions.get('window').height/100*18,
                alignSelf:'center'
            }}
            resizeMode={'contain'}
            />
            // :<View/>
        }
            <Text style={{fontFamily:'Kanit-Regular',fontSize:Dimensions.get('window').height/100*3,color:'#fff',alignSelf:'center',marginTop:Dimensions.get('window').height/100*2}}></Text>
      </LinearGradient>
    )
  }
}

const styles = {
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor:'black'
  }
}
const mapStateToProps = state => {
  return {
      checkIpnoe:state.check.checkIpnoe,
  }
}
export default connect(mapStateToProps,actions)(Splash)