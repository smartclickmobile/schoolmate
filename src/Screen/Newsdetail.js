
import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,Dimensions,TouchableOpacity,ScrollView,Image} from 'react-native';
import {  Thumbnail } from 'native-base';
import { MainColor } from '../../Assets/color';
import * as actions from "../Actions"
import {connect} from "react-redux"
import HTML from 'react-native-render-html';
import moment from "moment"
const htmlContent = `
    <h1>This HTML snippet is now rendered with native components !</h1>
    <h2>Enjoy a webview-free and blazing fast application</h2>
    <img src="https://i.imgur.com/dHLmxfO.jpg?2" />
    <em style="textAlign: center;">Look at how happy this native cat is</em>
`;
class Newsdetail extends Component {
    constructor(props){
        super(props)
        this.state = {
            data: ''
        }
    }
    
    componentDidMount(){
      // console.warn('componentDidMount',this.props.data)
      this.props.getNewsdetail(this.props.data)
    }
    componentWillReceiveProps(nextProps){
      // console.warn(nextProps.newsdetail)
      if(nextProps.newsdetail != null && nextProps.newsdetail != undefined){
        this.setState({data:nextProps.newsdetail})
      }
    }
  render() {
    // console.warn(this.state.data)
    return (
      <ScrollView style={styles.container}>
        <Text style={{fontFamily:'Kanit-Bold',fontSize:Dimensions.get('window').height/100*3}}>{this.state.data.title + ' (' +moment(this.state.data.published_to).format('DD-MM-YYYY')+')'}</Text>
        <HTML html={this.state.data.description} imagesMaxWidth={Dimensions.get('window').width} />
      </ScrollView>
    );
  }
}
const mapStateToProps = state => {
  return {
      NewsSuccess: state.News.NewsSuccess,
      newsdetail:state.News.newsdetail
      // error: state.Register.error
  }
}
export default connect(mapStateToProps, actions)(Newsdetail)
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5FCFF',
    padding:10
  },
});
