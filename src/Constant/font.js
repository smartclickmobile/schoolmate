import {
    Platform, 
    Dimensions,
  } from 'react-native';
export const SIZE_2_5 = Platform.isPad?Dimensions.get('window').width/100*1.5:Dimensions.get('window').width/100*2.5
export const SIZE_2_8 = Platform.isPad?Dimensions.get('window').width/100*1.8:Dimensions.get('window').width/100*2.8
export const SIZE_4 = Platform.isPad?Dimensions.get('window').width/100*3:Dimensions.get('window').width/100*4
export const SIZE_4_5 = Platform.isPad?Dimensions.get('window').width/100*3.5:Dimensions.get('window').width/100*4.5
export const SIZE_9 = Platform.isPad?Dimensions.get('window').width/100*8:Dimensions.get('window').width/100*9

export const ICON_SIZE = Platform.isPad?Dimensions.get('window').width/100*5.5:Dimensions.get('window').width/100*7