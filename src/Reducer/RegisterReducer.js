import { 
    RegisterSuccess,RegisterError
   } from "../Actions/type"
const INITIAL_STATE = {
    RegisterSuccess:[],
    error:null,
}
    export default (state = INITIAL_STATE, action) => {
      switch (action.type) {
        case RegisterSuccess:
          return {
            ...state,
            RegisterSuccess: action.payload,
            error: null
          }
        case RegisterError:
          return {
            ...state,
            RegisterSuccess: [],
            error: action.payload
          }
        default:
          return state
      }
    }