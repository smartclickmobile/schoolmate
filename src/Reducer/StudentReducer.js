import { 
    StudentError,StudentSuccess,
    EnrollError, EnrollSuccess,
    GradeSuccess,GradeError,
    LOADING
   } from "../Actions/type"
const INITIAL_STATE = {
    StudentSuccess:null,
    EnrollSuccess:null,
    error:null,
    grade:null,
    loading:false
}
    export default (state = INITIAL_STATE, action) => {
      switch (action.type) {
        case StudentSuccess:
          return {
            ...state,
            StudentSuccess: action.payload,
            loading:false,
            error: null
          }
        case StudentError:
          return {
            ...state,
            StudentSuccess: null,
            error: action.payload
          }
        case EnrollSuccess:
          return {
            ...state,
            EnrollSuccess: action.payload,
            loading:false,
            error: null
          }
        case EnrollError:
          return {
            ...state,
            EnrollSuccess: null,
            error: action.payload
          }
        case GradeSuccess:
        // console.warn('action.payload',action.payload)
          return {
            ...state,
            grade: action.payload,
            loading:false,
            error: null
          }
        case GradeError:
          return {
            ...state,
            grade: null,
            error: action.payload
          }
        case LOADING:
          return {
            ...state,
            loading: true,
          }
        default:
          return state
      }
    }