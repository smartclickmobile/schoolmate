import { combineReducers } from "redux"
import RegisterReducer from './RegisterReducer'
import LoginReducer from './LoginReducer'
import StudentReducer from './StudentReducer'
import NewsReducer from './NewsReducer'
import CalendarReducer from './CalendarReducer'
import CheckReducer from './CheckReducer'
export default combineReducers({
    // Reducer
    Register:RegisterReducer,
    Login:LoginReducer,
    student:StudentReducer,
    News: NewsReducer,
    Calendar: CalendarReducer,
    check:CheckReducer
  })