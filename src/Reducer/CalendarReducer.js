import { 
    CalendarSuccess, CalendarError,
    LOADINGCAL
   } from "../Actions/type"
const INITIAL_STATE = {
    CalendarSuccess:null,
    error:null,
    loading:false
}
    export default (state = INITIAL_STATE, action) => {
      switch (action.type) {
        case CalendarSuccess:
          return {
            ...state,
            CalendarSuccess: action.payload,
            loading:false,
            error: null
          }
        case CalendarError:
          return {
            ...state,
            CalendarSuccess: null,
            loading:false,
            error: action.payload
          }
        case LOADINGCAL:
        return {
          ...state,
          loading:true
        }
        default:
          return state
      }
    }