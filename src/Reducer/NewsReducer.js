import { 
    NewsSuccess, NewsError,
    NewsdetailSuccess, NewsdetailError,
    LOADINGNEWS
   } from "../Actions/type"
const INITIAL_STATE = {
    NewsSuccess:null,
    newsdetail:null,
    error:null,
    loading:false
}
    export default (state = INITIAL_STATE, action) => {
      switch (action.type) {
        case NewsSuccess:
          return {
            ...state,
            NewsSuccess: action.payload,
            loading:false,
            error: null
          }
        case NewsError:
          return {
            ...state,
            NewsSuccess: null,
            loading:false,
            error: action.payload
          }
        case NewsdetailSuccess:
          return {
            ...state,
            newsdetail: action.payload,
            loading:false,
            error: null
          }
        case NewsdetailError:
          return {
            ...state,
            newsdetail: null,
            loading:false,
            error: action.payload
          }
        case LOADINGNEWS:
          return {
            ...state,
            loading: true,
          }
        default:
          return state
      }
    }