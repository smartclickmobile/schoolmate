import { 
    LoginSuccess, LoginError,
    LogoutSuccess, LogoutError,
    UserSuccess, UserError
   } from "../Actions/type"
const INITIAL_STATE = {
    error:null,
    userinfo:null
}
    export default (state = INITIAL_STATE, action) => {
      switch (action.type) {
        case LoginSuccess:
          return {
            ...state,
            error: null
          }
        case LoginError:
          return {
            ...state,
            error: action.payload
          }
        case LogoutSuccess:
          return {
            ...state,
            error: null
          }
        case LogoutError:
          return {
            ...state,
            error: action.payload
          }
        case UserSuccess:
        // console.warn('UserSuccess',action.payload)
          return {
            ...state,
            userinfo:action.payload,
            error: null
          }
        case UserError:
          return {
            ...state,
            error: action.payload
          }
        default:
          return state
      }
    }