import React, { Component } from 'react'
import {
    Text,
    TouchableOpacity,
    Image,
    AsyncStorage,
    PermissionsAndroid,
    View,
    Platform,
    Dimensions
  } from "react-native"
import Icons from "react-native-vector-icons/FontAwesome"
import { Scene, Router, Stack, Drawer, Actions } from "react-native-router-flux"
import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions'
import { MainColor,MenuColor_Text } from '../Assets/color'
import { SIZE_4_5 } from './Constant/font'

import Splash from './Screen/Splash'
import SideBar from './Screen/SideBar'
import Dashborad from './Screen/Dashborad'
import Calendarscreen from './Screen/Calendarscreen'
import News from './Screen/News'
import Grade from './Screen/Grade'
import Login from './Screen/Login'
import Register from './Screen/Register'
import NewsDetail from './Screen/Newsdetail'
import Notification from './Screen/Notification'
import InfoStudent from './Screen/InfoStudent'
import {connect} from "react-redux"
import * as actions from "./Actions"
class Routers extends Component {
    _DrawerIcon = () => {
        return (
            <View style={{flex:1}}>
                <Icons name="bars" color={MenuColor_Text} size={Platform.isPad?40: 24} style={{marginTop:Dimensions.get('window').height/100*2.5 }} />
            </View>
        )
      }
    _Navigation = data => {
        const height = Dimensions.get('window').height/100*7
        return {height: height,backgroundColor:data}
    }
    _Title = data => {
        return(
            <View style={{flex:1}}>
                <Text style={{fontFamily:'Kanit-Regular',fontSize:SIZE_4_5,color:MenuColor_Text,textAlign:'center'}}>{data}</Text>
            </View>
        )
    }
    _onrefresh(){
        console.warn('rightbutton',Actions.currentScene)
        if(Actions.currentScene == '_dashborad' || Actions.currentScene == 'dashborad'){
            this.props.setLoading()
            this.props.getStudent()
            this.props.getEnroll()
            this.props.getGrade()
            // Actions.refresh()
            
        }else if(Actions.currentScene == '_calendarscreen' || Actions.currentScene == 'calendarscreen'){
            this.props.setLoadingCal()
            this.props.getCalendar()
        }else if(Actions.currentScene == '_news' || Actions.currentScene == 'news'){
            this.props.setLoadingNews()
            this.props.getNews()
        }else if(Actions.currentScene == '_notification' || Actions.currentScene == 'notification'){

        }
    }
    _RightButton = data => {
      
        return (
            <View style={{flexDirection:'row'}}>
                <TouchableOpacity style={{flex:1,width:Platform.isPad?60: 40}} onPress={()=>this._onrefresh()}>
                    <Icons name="refresh" color={MenuColor_Text} size={Platform.isPad?40: 24} style={{alignSelf:'center'}}/>
                </TouchableOpacity>
                <TouchableOpacity style={{flex:1,width:Platform.isPad?60: 40}} onPress={()=>Actions.notification()}>
                    <Icons name="bell" color={MenuColor_Text} size={Platform.isPad?40: 24} style={{alignSelf:'center'}}/>
                </TouchableOpacity>
            </View>
            
        )
    }
    _RightButton1 = data => {
        return (
            <View style={{flex:1}}>
               
            </View>
        )
    }
    _LeftButton = data => {
        return (
            <TouchableOpacity style={{flex:1,width:40}} onPress={()=>Actions.pop()}>
                <Icons name="chevron-left" color={MenuColor_Text} size={24} style={{alignSelf:'center'}}/>
            </TouchableOpacity>
        )
    }
    render(){
        return(
            
            <Router>
            <Stack key='root'>
                <Scene key="splash" component={Splash} title="App" hideNavBar timeout={0} initial />
                <Scene key="login" component={Login} hideNavBar/>
                <Scene 
                    key="register" 
                    component={Register} 
                    renderTitle={this._Title('สมัครสมาชิก')} 
                    navigationBarStyle={this._Navigation(MainColor)}
                    renderRightButton={this._RightButton1}
                    renderLeftButton={this._LeftButton}
                    hideNavBar
                />
                <Scene 
                    key="newsdetail" 
                    component={NewsDetail} 
                    renderTitle={this._Title('ข่าวประชาสัมพันธ์')} 
                    // navigationBarStyle={this._Navigation('#908CE5')}
                    navigationBarStyle={this._Navigation(MainColor)}
                    renderRightButton={this._RightButton1}
                    renderLeftButton={this._LeftButton}
                />
                <Scene 
                    key="infostudent" 
                    component={InfoStudent} 
                    renderTitle={this._Title('ข้อมูลนักเรียน')} 
                    navigationBarStyle={this._Navigation(MainColor)}
                    renderRightButton={this._RightButton1}
                    renderLeftButton={this._LeftButton}
                />
                <Scene 
                    key="grade" 
                    component={Grade} 
                    renderTitle={this._Title('ผลการศึกษา')}
                    navigationBarStyle={this._Navigation(MainColor)}
                    renderRightButton={this._RightButton1}
                    renderLeftButton={this._LeftButton}
                />
                <Scene
                    ref="drawerRefs"
                    key="drawer"
                    drawer
                    drawerWidth={Platform.isPad?Dimensions.get('window').width/100*50:Dimensions.get('window').width/100*70}
                    contentComponent={SideBar}
                    drawerIcon={this._DrawerIcon}
                    navigationBarStyle={this._Navigation(MainColor)}
                    renderRightButton={this._RightButton}
                    hideNavBar
                >
                
                    <Scene key="dashborad" component={Dashborad} renderTitle={this._Title('หน้าหลัก')}/>
                    <Scene key="calendarscreen" component={Calendarscreen} renderTitle={this._Title('ปฏิทินโรงเรียน')}/>
                    <Scene 
                        key="news"  
                        component={News}  
                        // navigationBarStyle={this._Navigation('#908CE5')} 
                        navigationBarStyle={this._Navigation(MainColor)}
                        renderTitle={this._Title('ข่าวประชาสัมพันธ์')}
                    />
                    
                    <Scene key="notification" component={Notification} renderTitle={this._Title('กล่องแจ้งเตือน')}/>
                    
                </Scene>
                
            </Stack>
            </Router>
        )
    }
   
}
const mapStateToProps = state => {
    return {
    studenterror: state.student.error,
    }
  }
  
  export default connect(mapStateToProps, actions)(Routers)